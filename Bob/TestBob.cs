
using System;
using NUnit.Framework;

namespace Bob
{

	[TestFixture]
	public class TestBob
	{

		[Test]
		public void BobAnsweresToQuestionMark()
		{	
			string expectedAnswer = "Sure.";
			string answer = Bob.respondsTo("?");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnsweresToQuestion()
		{	
			string expectedAnswer = "Sure.";
			string answer = Bob.respondsTo("Hey Bob?");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnsweresToAnotherQuestion()
		{	
			string expectedAnswer = "Sure.";
			string answer = Bob.respondsTo("Are you sure that you considered all kinds of questions?");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnsweresToNothing()
		{	
			string expectedAnswer = "Fine. Be that way!";
			string answer = Bob.respondsTo("");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnsweresToExclamation()
		{	
			string expectedAnswer = "Whatever.";
			string answer = Bob.respondsTo("!");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnsweresToABCDE()
		{	
			string expectedAnswer = "Whatever.";
			string answer = Bob.respondsTo("abcde");
			Assert.AreEqual(expectedAnswer, answer);	
		}
		
		[Test]
		public void BobAnsweresToSpace()
		{	
			string expectedAnswer = "Whatever.";
			string answer = Bob.respondsTo(" ");
			Assert.AreEqual(expectedAnswer, answer);
		}	
		
		[Test]
		public void BobGetsShoutedAtWithCapitalLettersAndWithExclamationMarks()
		{	
			string expectedAnswer = "Whoa, chill out.";
			string answer = Bob.respondsTo("HEY BOB !!!");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		
		[Test]
		public void BobGetsShoutedAtWithCapitalLettersAndWithoutExclamationMarks()
		{	
			string expectedAnswer = "Whoa, chill out.";
			string answer = Bob.respondsTo("HEY BOB");
			Assert.AreEqual(expectedAnswer, answer);
		}	
		
		[Test]
		public void BobAnswersToHeyBob()
		{	
			string expectedAnswer = "Whatever.";
			string answer = Bob.respondsTo("Hey Bob!");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnswersToHeyBobWithSpaceAtFront()
		{	
			string expectedAnswer = "Whoa, chill out.";
			string answer = Bob.respondsTo(" HEY BOB ");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
		public void BobAnswersToHEY1BOB()
		{	
			string expectedAnswer = "Whoa, chill out.";
			string answer = Bob.respondsTo("HEY1BOB");
			Assert.AreEqual(expectedAnswer, answer);
		}
	
	
		[Test]
		public void BobAnsweresToQuestionAdditionalSpace()
		{	
			string expectedAnswer = "Sure.";
			string answer = Bob.respondsTo("Hey Bob? ");
			Assert.AreEqual(expectedAnswer, answer);
		}
		
		[Test]
    	public void Shouting_with_special_characters()
    	{
        	string expectedAnswer = "Whoa, chill out.";
			string answer = Bob.respondsTo("ZOMG THE %^*@#$(*^ ZOMBIES ARE COMING!!11!!1!");
			Assert.AreEqual(expectedAnswer, answer);
		}	
	}	
}