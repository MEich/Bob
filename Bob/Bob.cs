
using System;
using System.Text.RegularExpressions;

namespace Bob
{


	public class Bob
	{

		public static string respondsTo(string whatISay)
		{
			if (isAQuestionToBob(whatISay))
			{
				return "Sure.";
			} 
			else if (isSayingNothingToBob(whatISay))
			{
				return "Fine. Be that way!";	
			} 
			else if (isYellingAtBob(whatISay))
			{		
				return "Whoa, chill out.";
			}
			else 
			{
				return "Whatever.";
			}

		}
		public static bool isAQuestionToBob(string input)
		{
			return input.Trim().EndsWith("?");
		}	
			
		public static bool isYellingAtBob(string input)
		{
			return Regex.IsMatch(input, @"^(.*[A-Z][^a-z].*)$");
		}	
		
		public static bool isSayingNothingToBob(string input)
		{
			return input == "";
		}	
			
	}	
}	

